function filter(elements, cb) {

    if (elements === undefined || Array.isArray(elements) === false || elements.length === 0 || cb === undefined || typeof cb != "function") {
        return;
    }

    let filteredElements = []
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            filteredElements.push(elements[index]);
        }
    }
    return filteredElements;
}

module.exports = filter;