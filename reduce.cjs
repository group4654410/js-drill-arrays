function reduce(elements, cb, startingValue) {

    if (elements === undefined || Array.isArray(elements) === false || elements.length === 0 || cb === undefined || typeof cb != "function") {
        return;
    }

    let accumulator = startingValue !== undefined ? startingValue : elements[0];
    for (let index = startingValue !== undefined ? 0 : 1; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements);
    }
    return accumulator;
}

module.exports = reduce;