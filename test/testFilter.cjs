let assert = require("assert");
const filter = require("../filter.cjs");

const items = [1, 2, 3, 4, 5, 5];
let result = filter(items, (a) => { return a > 2 });
assert.deepEqual([3, 4, 5, 5], result);