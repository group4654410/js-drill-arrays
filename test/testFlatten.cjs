let assert = require("assert");
const flatten = require("../flatten.cjs");

const nestedArray = [1, [2], [[3]], [[[4]]]];
let result = flatten(nestedArray);
assert.deepEqual([1, 2, 3, 4], result);