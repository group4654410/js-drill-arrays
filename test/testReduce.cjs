let assert = require("assert");
const reduce = require("../reduce.cjs");

const items = [1, 2, 3, 4, 5, 5];
let result = reduce(items, (acc, val, index, assert) => {return acc + val}, 6);
assert.deepEqual(26, result);