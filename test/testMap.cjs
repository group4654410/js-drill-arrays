let assert = require("assert");
const map = require("../map.cjs");

const items = [1, 2, 3, 4, 5, 5];
let result = map(items, (a) => a*a);
assert.deepEqual([1, 4, 9, 16, 25, 25], result);