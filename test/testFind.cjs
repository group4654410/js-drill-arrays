let assert = require("assert");
const find = require("../find.cjs");

const items = [1, 2, 3, 4, 5, 5];
let result = find(items, (a) => {return (a === 3)});
assert.deepEqual(3, result);