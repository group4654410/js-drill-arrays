function map(elements, cb) {

    if (elements === undefined || elements.length === 0 || cb === undefined || typeof cb != "function") {
        return [];
    }

    let mappedElements = []
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements)) {
            mappedElements.push(cb(elements[index], index, elements));
        } else {
            mappedElements.push(cb({ key, value }, index, elements));
        }
    }
    return mappedElements;
}

module.exports = map;