function flatten(elements, depth = 1) {

    if (elements === undefined || Array.isArray(elements) === false || elements.length === 0) {
        return [];
    }

    let flattenedElements = []
    for (const element of elements) {
        if (element === undefined) {
            continue;
        }

        if (Array.isArray(element) && depth > 0) {
            if (depth === Infinity) {
                flattenedElements.push(...flatten(element, Infinity));
            } else {
                flattenedElements.push(...flatten(element, depth - 1));
            }
        } else {
            flattenedElements.push(element);
        }
    }
    return flattenedElements;
}

module.exports = flatten;