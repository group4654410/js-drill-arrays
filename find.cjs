function find(elements, cb) {

    if (elements === undefined || Array.isArray(elements) === false || elements.length === 0 || cb === undefined || typeof cb != "function" || cb.length != 1) {
        return;
    }

   for(const element of elements) {
        if (cb(element)){
            return element;
        }
    };
    return;
}

module.exports = find;