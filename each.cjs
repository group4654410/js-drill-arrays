function each(elements, cb) {

    if (elements === undefined || Array.isArray(elements) === false || elements.length === 0 || cb === undefined || typeof cb != "function" || cb.length != 2) {
        return;
    }

    for(let index = 0; index < elements.length; index++){
        cb(elements[index], index);
    }
}

module.exports = each;