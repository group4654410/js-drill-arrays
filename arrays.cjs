const each = require("./each.cjs");
const map = require("./map.cjs");
const reduce = require("./reduce.cjs");
const find = require("./find.cjs");
const filter = require("./filter.cjs");
const flatten = require("./flatten.cjs");

// each([1, 2, 3], (a, index) => console.log(a, index));
// let result = map([1, 2, 3], (a, i) => {return a*i});
// let result = reduce([1, 2, 3, 4], (acc, val) => {return acc + val}, 6);
// let result = find([1, 2, 3, 4, 5], (a) => {return (a === 0)});
// console.log(filter([1, 2, 3, 4, 5], (a) => {return a > 2}));
// console.log(flatten([[[1, 2, 3, [[4]]]]]));